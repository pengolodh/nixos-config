# drive config
{
  lib,
  extraVar,
  ...
}: {
  # as this is vm we will use disko to format the disk, this would take definition with UUID for a system with multible os's (like nixos-laptop-asus for example)
  disko.devices = lib.mkForce {
    disk = {
      root = {
        type = "disk";
        device = extraVar.disks.linuxRoot; # this selects the first entry in the disks array that we defined in ${self}/hosts/default.nix
        content = {
          type = "table"; # set the partition table
          format = "msdos";
          partitions = [
            {
              # we are using a legacy table so the partiions will be defined as
              name = "NIXOS_MAIN";
              part-type = "primary";
              start = "1M";
              end = "100%"; # use 100% of remaining diskspace in ${diskname}
              content = {
                type = "btrfs";
                extraArgs = ["-f"]; # Override existing partitions
                subvolumes = {
                  "/boot" = {
                    mountpoint = "/boot";
                  };
                  "/root" = {
                    mountpoint = "/";
                    mountOptions = ["compress=zstd" "noatime"];
                  };
                  "/nix" = {
                    mountpoint = "/nix";
                    mountOptions = ["compress=zstd" "noatime"];
                  };
                  "/home" = {
                    mountpoint = "/home";
                    mountOptions = ["compress=zstd" "noatime"];
                  };
                  # "home/pengolodh" {}; # Sub(sub)volume doesn't need a mountpoint as its parent is mounted

                  # impermanence
                  "/persist" = {
                    mountpoint = "/persist";
                    mountOptions = ["compress=zstd" "noatime"];
                  };

                  "/swap" = {
                    mountpoint = "/.swapvol"; # make it hidden cuz it has no use not being so
                    swap = {
                      swapfile.size = "4G";
                      # you can declare multible swapfiles, idk why you would do that...
                    };
                  };
                };
                postCreateHook = "mount ${extraVar.disks.linuxRoot}-part1 /mnt ; btrfs subvolume snapshot -r /mnt/root /mnt/root-blank; umount /mnt"; # create the initial empty subvolume snapshot that we will return to at each boot
              };
            }
            # declare more partitons here:
          ];
        };
      };
    };
    # use more disks here:
  };
  fileSystems = {
    "/persist".neededForBoot = true;
  };

  # we are using btrfs so we can enable the scrub service here, as it is filesystem dependant
  services.btrfs.autoScrub = {
    enable = true;
    interval = "weekly";
    fileSystems = ["/"]; # if home is on a separate drive or not a subvolume of another location then you can add it to the list (unlike above where /nix and /home are nested subvolumes under / you only need to scrub / -> scrub is per drive or whole partition)
  };
  # ---
}
