let
  users = (import ../../identities.nix).users;
  systems = (import ../../identities.nix).systems;
  allUsers = builtins.attrValues users;
  allSystems = builtins.attrValues systems;
in {
  "password.age".publicKeys = [users.pengolodh] ++ allSystems; # the list tells agenix to encrypt the file with the specified public keys
}
