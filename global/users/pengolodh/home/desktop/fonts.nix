{pkgs, ...}: {
  home.packages = with pkgs; [
    corefonts # ms fonts
  ];
  fonts.fontconfig.enable = true; # enabling this auto-detects installed fonts in home.packages and environment.packages

  stylix.fonts = {
    sansSerif = {
      name = "UbuntuSans Nerd Font Med";
      package = pkgs.nerd-fonts.ubuntu-sans;
    };
    serif = {
      name = "Ubuntu Nerd Font Med";
      package = pkgs.nerd-fonts.ubuntu;
    };
    monospace = {
      name = "UbuntuSansMono Nerd Font Bold";
      package = pkgs.nerd-fonts.ubuntu-sans;
    };
    emoji = {
      name = "Noto Color Emoji";
      package = pkgs.noto-fonts-color-emoji;
    };
    sizes = {
      applications = 13;
      terminal = 14;
      desktop = 13;
      popups = 13;
    };
  };
}
