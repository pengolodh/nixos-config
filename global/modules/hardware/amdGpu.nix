{
  pkgs,
  extraVar,
  ...
}: {
  #boot.initrd.kernelModules = ["amdgpu"];
  boot.kernelParams = ["amdgpu.dpm=1" "amdgpu.ppfeaturemask=0xffffffff"];
  # -> amdgpu.dpm=1 enables amd dynamic power management,
  # ->  amdgpu.ppfeaturemask=0xffffffff is to enable overclocking support (powerplay)

  services.xserver.videoDrivers = ["amdgpu"];

  hardware = {
    amdgpu = {
      initrd.enable = true; # adds boot.initrd.kernelModules = ["amdgpu"];
      opencl.enable = true; # adds the rocm stack (clr and clr.icd)
    };
    graphics = {
      # enable mesa drivers:
      enable = true;
      enable32Bit = true;
    };
  };
  environment = {
    variables = {
      # Configure AMD. Only required if you have AMD iGPU and/or dGPU
      # "AMDVLK" = AMD's Vulkan driver
      # "RADV" = mesa's RADV driver (recommended)
      AMD_VULKAN_ICD = "RADV";
      # Enable raytracing (VKD3D-proton). Recommended with RADV above (not AMDVLK).
      VKD3D_CONFIG = "dxr,dxr11";
      RADV_PERFTEST = "rt";
      ## -> these are from # from https://asus-linux.org/blog/updates-2022-04-16/
      # rocm related
      ROCR_VISIBLE_DEVICES = extraVar.hardware.rocmgpu;
    };
    systemPackages = with pkgs; [
      rocmPackages.rocm-smi
      rocmPackages.rocminfo
      clinfo
      nvtopPackages.amd
    ];
  };
  # systemd-rules
  systemd.tmpfiles.rules = [
    "L+    /opt/rocm/hip   -    -    -     -    ${pkgs.rocmPackages.clr}" # Most software has the HIP libraries hard-coded. You can work around it on NixOS by using this
  ];
}
