{
  lib,
  extraVar,
  ...
}: {
  # note that the filesytem needs to be setup for impernamence, for example tmpfs as /
  # if the system uses a btrfs snapshotting or zfs snapshotting ephemeral system that will need to be specified in the host/impermanence.nix file ( as that is host specific)
  # the reason why you wouldnt want tmpfs for / on all systems is that it requires more memory
  # we use disko to define the system disks we want nixos to be installed on, this way they will be partitioned automatically when we use a tool like nixos-anywhere
  disko.devices = {
    disk.nixosRoot = lib.mkDefault {
      device = extraVar.disks.linuxRoot;
      type = "disk";
      content = {
        type = "gpt";
        partitions = {
          ESP = {
            name = "NIXOS_EFI";
            size = "512M";
            type = "EF00"; # efi partition type
            content = {
              # here we will tell it the filesystem type and mountpoint
              type = "filesystem";
              format = "vfat";
              mountpoint = "/boot";
              mountOptions = ["defaults"];
            };
          };
          cr_nixosRoot = {
            size = "100%";
            content = {
              type = "luks";
              name = "cr_nixosRoot";
              settings.allowDiscards = true;
              passwordFile = "/tmp/nixosRoot.passwd"; # if you want this to be a password use echo -n "password" > /tmp/nixos-main.key , the -n is very important as it removes the trailing newline, the /tmp is only for the installer, this file is only used when the disk is partitioned by disko
              # no keyfile will be specified as there will only be a password for this disk
              content = {
                type = "filesystem";
                format = "ext4";
                mountpoint = "/nix";
              };
            };
          };
        };
      };
    };
    nodev."/" = {
      fsType = "tmpfs";
      mountOptions = [
        "size=8G"
        "defaults"
        "mode=755"
      ];
    }; # root will be on a tmpfs, it can grow to a maximum of 8G, this should be plenty
  };
  fileSystems = lib.mkDefault {
    "/".neededForBoot = true;
    "/nix".neededForBoot = true;
  };
}
