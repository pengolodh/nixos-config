{
  config,
  lib,
  extraVar,
  secureBoot,
  ...
}: {
  # tell agenix that keys are stored in persist/ssh
  age.identityPaths = [
    "${extraVar.persistPath}/ssh/ssh_host_ed25519_key"
  ];
  # openssh can create its own keys if they do not exist yet
  services.openssh = {
    enable = true;
    # sets nix to use/generate the host keys in the given directory
    hostKeys = [
      {
        path = "${extraVar.persistPath}/ssh/ssh_host_ed25519_key";
        type = "ed25519";
      }
      {
        path = "${extraVar.persistPath}/ssh/ssh_host_rsa_key";
        type = "rsa";
        bits = 4096;
      }
    ];
  };
  # same with wireguard
  /*
  networking.wireguard.interfaces.wg0 = lib.mkIf (!config.systemd.network.enable) {
    generatePrivateKeyFile = true;
    privateKeyFile = "${extraVar.persistPath}/etc/wireguard/wg0";
  };
  */
  # persist these directories and files
  environment.persistence.${extraVar.persistPath} = {
    hideMounts = true; # For added "security" and less clutter in the system
    directories =
      [
        "/var/log" # perserve the system logs
        "/var/lib/systemd/coredump" # perserve the coredump
        "/var/lib/nixos"
        "/var/lib/nixos-containers"
        "/var/lib/machines"

        (lib.mkIf secureBoot "/etc/secureboot")
        (lib.mkIf config.hardware.bluetooth.enable "/var/lib/bluetooth")
        (lib.mkIf config.virtualisation.docker.enable "/var/lib/docker")
        (lib.mkIf config.networking.networkmanager.enable "/etc/NetworkManager/system-connections")

        (lib.mkIf config.services.upower.enable "/var/lib/upower")
        (
          lib.mkIf config.services.colord.enable
          {
            directory = "/var/lib/colord";
            user = "colord";
            group = "colord";
            mode = "u=rwx,g=rx,o=";
          } # colord map needs to belong to the colord user
        )
      ]
      ++ lib.optionals config.virtualisation.libvirtd.enable ["/var/lib/libvirt" "/etc/libvirt"];
    files =
      [
        "/etc/adjtime" # # systemd ntp time adjust
        "/etc/machine-id"
        #(lib.mkIf (!config.nix-mineral.enable) "/etc/machine-id") # -> the nix-mineral module uses a pregenerated machine-id
        (lib.mkIf config.services.locate.enable "/var/cache/locatedb") # locatedb cache, the service seems to be unable to replace this file for some reason (ownership maybe?)
      ]
      ++ lib.optionals
      config.networking.networkmanager.enable
      [
        "/var/lib/NetworkManager/secret_key"
        "/var/lib/NetworkManager/seen-bssids"
        "/var/lib/NetworkManager/timestamps"
      ]
      ++ lib.optionals
      config.users.mutableUsers
      [
        "/etc/passwd"
        "/etc/shadow"
      ];
  };

  # disable sudo lecture, it is the warning message that displays the first time you use the sudo command, with ephemeral setups it does that each time
  security.sudo.extraConfig = lib.mkIf config.security.sudo.enable ''
    # rollback results in sudo lectures after each reboot
    Defaults lecture = never
  '';
}
